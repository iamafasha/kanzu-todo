(function() {

    class TaskList {

        tasks  = [
            {
                id: this.uuidv4(),
                name: 'Buy groceries for next week',
                completed: true,
                lastEdited: new Date(2024, 4, 19 ),
                dueDate: new Date(2024, 4, 14)
            },
            {
                id: this.uuidv4(),
                name: 'Renew car insurance',
                completed: true,
                lastEdited: new Date(2024, 4, 19 ),
                dueDate: new Date(2024, 4, 14)
            },
            {
                id: this.uuidv4(),
                name: 'Sign up for online course',
                completed: true,
                lastEdited: new Date(2024, 4, 19 ),
                dueDate: new Date(2024, 4, 14)
            }
        ]

        constructor(id) {
          this.list = document.getElementById(id);
          this.renderTasks()
        }

        renderTasks() {
            this.list.innerHTML = "";
            this.tasks.forEach(task => {
                this.addTasktoDom(task);
            })
        }
        addTask(text) {
            if(text == ""){
                return 
            }
            this.tasks.push({
                id: this.uuidv4(),
                name: text,
                completed: false,
                lastEdited: new Date(2024, 4, 19 ),
                dueDate: new Date(2024, 4, 14)
            })
            this.renderTasks();
        }

        createTaskHTML(task) {
            const lastEdited = task.lastEdited;
            const shortFormat = `${lastEdited.getMonth() + 1}/${lastEdited.getDate()}/${lastEdited.getFullYear()}`;
            return `<li class="task-item" id="${task.id}"> 
                <label>
                    <div class="item-label">
                        <input class="item-checkbox" type="checkbox" name="checkbox"   />
                        <div id="editiable-${task.id}"> ${task.name} </div> 
                    </div>
                </label>
                <div class="actions " >

                    <div>
                        <i  class="fa-solid fa-trash-can deleteIcon" id="deleteIcon-${task.id}" ></i>
                        <i   class="fa-solid fa-pencil editIcon" id="editIcon-${task.id}" ></i>   
                    </div>

                    <div class="last-edit">
                        <i class="fas fa-exclamation-circle"></i>
                        <small>${shortFormat}</small>
                    </div>
                                                
                </div>
            </li>`;
        }

        uuidv4() {
            return "10000000-1000-4000-8000-100000000000".replace(/[018]/g, c =>
                (+c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> +c / 4).toString(16)
            );
        }   
        
        // Delete tasks
        deleteTask(id) {
            this.tasks = this.tasks.filter(t => t.id !== id)
            this.renderTasks();
        }

        // Edit tasks
        editTask(id) {
            
            const element = document.getElementById(`editiable-${id}`)
            //convert this into an input
            const input = document.createElement('input');
            input.setAttribute('type', 'text');
            input.setAttribute('id', `editiable-${id}`);
            input.setAttribute('value', element.textContent);
            input.setAttribute('class', 'edit-input');
            input.addEventListener('change', e => this.updateTask(id, e.target.value))
            element.parentNode.replaceChild(input, element);
            input.focus();
            debugger
            //move cusrsor to the right
            input.setSelectionRange(input.value.length, input.value.length);

            input.addEventListener('blur', e => {
                
                const task = this.tasks.find(task => task.id == id)
                if(e.target.textContent != task.name){
                    this.updateTask(id, e.target.value)
                    this.renderTasks();
                }
            })
        }

        updateTask(id, taskContent) {
            const task = this.tasks.find(task => task.id == id)
            task.name = taskContent;
            task.lastEdited = new Date();
        }

        addTasktoDom (task){
            const taskHTML = this.createTaskHTML(task);
                const taskElement = document.createRange().createContextualFragment(taskHTML);
                this.list.appendChild(taskElement)  
                
                document.getElementById(`editIcon-${task.id}`).addEventListener('click', e => this.editTask(task.id))
                document.getElementById(`deleteIcon-${task.id}`).addEventListener('click', e =>this.deleteTask(task.id))
        }

     
       
    }
    
    const tasklist = new TaskList('task-list');

    const addTaskButton = document.querySelector('#add-task');

    addTaskButton.addEventListener('click', function(e) {
        e.preventDefault();
        const taskText = document.querySelector('#input-add').value;
        tasklist.addTask(taskText);
        document.querySelector('#input-add').value = '';
    })




})();
